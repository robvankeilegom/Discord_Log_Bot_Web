<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\Factory;

use App\Server;
use App\Channel;
use App\PrivateChannel;
use App\Member;
use App\Message;
use App\Discord_Role;
use App\Invite;
use App\Discord_Permission;
use App\Emoji;
use App\Ban;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $notifications = array();
        $roles = DB::connection("discord_log_db")->table('member_changes')->where('attribute', 'role')->whereDate('timestamp', '>=', date_format(Carbon::today()->subWeek()->subWeek(), 'Y-m-d'))
          ->select()->get();
        foreach ($roles as $role) {
          $notification = "";
          $notification .= (Discord_Role::find($role->before)) ? ("<a href=\"" . route('admin.roles.role', $role->before) . "\">" . Discord_Role::find($role->before)->name . "</a>") : "Unknown role";
          $notification .= ($role->after == "added") ?" assigned to " : "removed from";
          $notification .= "<a href=\"" . route('admin.members.member', $role->member_id) . "\">" .  Member::find($role->member_id)->name . "</a>";

          array_push($notifications, array("timestamp"=>$role->timestamp, "notification"=>$notification));
        }

        $members = DB::connection("discord_log_db")->table('member_changes')->where('attribute', 'joined')->orWhere('attribute', 'left')->whereDate('timestamp', '>=', date_format(Carbon::today()->subWeek(), 'Y-m-d'))
          ->get();
        foreach ($members as $member) {
          $notification = "";
          $notification .= (Member::find($member->member_id)) ? ("<a href=\"" . route('admin.members.member', $member->member_id) . "\">" . Member::find($member->member_id)->name . "</a> ") : "Unknown member ";
          $notification .= $member->attribute;
          $notification .= " <a href=\"" . route('admin.servers.server', $member->server_id) . "\">" .  Server::find($member->server_id)->name . "</a> ";

          array_push($notifications, array("timestamp"=>$member->timestamp, "notification"=>$notification));
        }



        usort($notifications, function ($a, $b) {
          if ($a['timestamp'] == $b['timestamp']) return 0;
          return ($a['timestamp'] > $b['timestamp']) ? -1 : 1;
        });
        view()->share('notifications', $notifications);


    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $counts = array();
        $counts["servers"] = Server::all()->count();
        $counts["channels"] = Channel::all()->count();
        $counts["members"] = Member::all()->count();
        $counts["messages"]["total"] = Message::all()->count();

        $voiceconnections = DB::connection("discord_log_db")->table('voice_changes')->where('before', 'None')->whereDate('timestamp', '>=', date_format(Carbon::today()->subWeek()->subWeek(), 'Y-m-d'))->groupBy(DB::raw('DATE(timestamp)'))
          ->select(DB::raw('DATE(timestamp) date'), DB::raw('COUNT(DATE(timestamp)) as count'))->get();

        $counts["voiceconnections"]["total"] = 0;
        $lastWeek = new \DatePeriod(Carbon::today()->subWeek()->subWeek(), \DateInterval::createFromDateString('1 day'), Carbon::today());

        foreach ($lastWeek as $day) {
            $counts["messages"][date_format($day, 'Y-m-d')] = Message::where('timestamp', 'like', date_format($day, 'Y-m-d') . '%')->count();

            $counts["voiceconnections"][date_format($day, 'Y-m-d')] = 0;
            foreach ($voiceconnections as $vc_day) {
              if ($vc_day->date == date_format($day, 'Y-m-d')) {
                $counts["voiceconnections"][date_format($day, 'Y-m-d')] = $vc_day->count;
                $counts["voiceconnections"]["total"] += $vc_day->count;
              }
          }
        }
        return view('la.dashboard', compact('counts'));
    }

    public function servers()
    {
      $servers = Server::Paginate(10);
      return view('servers.servers', compact('servers'));
    }

    public function servers_member($member_id = null)
    {
      if ($member_id) {
        $servers = Server::whereHas('members', function($member) use ($member_id) {
          $member->where('id', $member_id);
        })->get();
        return view('servers.serversmember', compact(['servers', 'member_id']));
      } else {
        return view('servers.serversmember');
      }

    }

    public function servers_server($server_id = null)
    {
      $server = Server::find($server_id);
      return view('servers.server', compact('server'));
    }

    public function channels()
    {
      $type = "";
      $channels = Channel::Paginate(10);
      return view('channels.channels', compact(['channels', 'type']));
    }

    public function voicechannels()
    {
      $type = "voice";
      $channels = Channel::where('type', 'voice')->Paginate(20);
      return view('channels.channels', compact(['channels', 'type']));
    }

    public function textchannels()
    {
      $type = "text";
      $channels = Channel::where('type', 'text')->Paginate(20);
      return view('channels.channels', compact(['channels', 'type']));
    }

    public function channels_channel($channel_id = null)
    {
      if ($channel_id) {
        $channel = Channel::find($channel_id);
        return view('channels.channel', compact(['channel']));
      } else {
        $channels = Channel::all();
        return view('channels', compact(['channels', 'type']));
      }
    }

    public function channels_server($server_id = null)
    {
      if ($server_id) {
        $channels = Server::find($server_id)->channels;
        return view('channels.channelsserver', compact(['channels', 'server_id']));
      } else {
        $channels = Channel::all();
        return view('channels', compact(['channels', 'type']));
      }

    }

    public function privatechannels()
    {
      $channels = PrivateChannel::Paginate(20);
      return view('channels.privatechannels', compact(['channels']));
    }

    public function members()
    {
      $members = Member::Paginate(10);
      return view('members.members', compact('members'));
    }

    public function members_member($member_id = null)
    {
      $member = Member::find($member_id);
      return view('members.member', compact('member'));
    }

    public function members_bots()
    {
      $members = Member::where('bot', 1)->Paginate(10);;
      return view('members.bots', compact('members'));
    }

    public function members_server($server_id = null)
    {
      if ($server_id) {
        $members = Server::find($server_id)->members()->Paginate(10);
        return view('members.membersserver', compact(['members', 'server_id']));
      } else {
        return view('members.members');
      }
    }

    public function members_channel($channel_id = null)
    {
      if ($channel_id) {
        $members = PrivateChannel::find($channel_id)->recipients()->Paginate(10);
        return view('members.memberschannel', compact(['members', 'channel_id']));
      } else {
        return view('members.members');
      }
    }

    public function members_role($role_id = null)
    {
      if ($role_id) {
        $members = Discord_Role::find($role_id)->members()->Paginate(20);
        return view('members.membersrole', compact(['members', 'role_id']));
      } else {
        return view('');
      }

    }

    public function roles()
    {
      $roles = Discord_Role::Paginate(15);
      return view('roles.roles', compact('roles'));
    }

    public function roles_role($role_id = null)
    {
      if ($role_id) {
        $role = Discord_Role::find($role_id);
        $permissions = Discord_Permission::find($role->permissions);
        return view('roles.role', compact(['role', 'permissions']));
      } else {
        $roles = Discord_Role::all();
        return view('roles.roles', compact(['roles']));
      }
    }

    public function roles_server($server_id = null)
    {
      if ($server_id) {
        $roles = Server::find($server_id)->roles()->Paginate(15);
        return view('roles.rolesserver', compact(['roles', 'server_id']));
      } else {
        $roles = Discord_Role::all();
        return view('roles.roles', compact(['roles']));
      }
    }

    public function roles_member($member_id = null)
    {
      if ($member_id) {
        $roles = Member::find($member_id)->roles()->Paginate(15);
        return view('roles.rolesmember', compact(['roles', 'member_id']));
      } else {
        $roles = Discord_Role::all();
        return view('roles.roles', compact(['roles']));
      }
    }

    public function invites()
    {
      $invites = Invite::orderBy('created_at', 'DESC')->Paginate(20);
      return view('invites.invites', compact(['invites']));
    }

    public function voice_activity()
    {
      $activities = DB::connection("discord_log_db")->table('voice_changes')->orderBy('timestamp', 'DESC')->simplePaginate(20);
      return view('activity.voice', compact(['activities']));
    }

    public function server_activity()
    {
      $activities = DB::connection("discord_log_db")->table('server_changes')->orderBy('timestamp', 'DESC')->simplePaginate(20);
      return view('activity.server', compact(['activities']));
    }

    public function member_activity()
    {
      $activities = DB::connection("discord_log_db")->table('member_changes')->orderBy('timestamp', 'DESC')->simplePaginate(20);
      return view('activity.member', compact(['activities']));
    }

    public function channel_activity()
    {
      $activities = DB::connection("discord_log_db")->table('channel_changes')->orderBy('timestamp', 'DESC')->simplePaginate(20);
      return view('activity.channel', compact(['activities']));
    }

    public function emojis()
    {
      $emojis = Emoji::Paginate(10);
      return view('emojis.emojis', compact('emojis'));
    }

    public function bans()
    {
      $bans = Ban::Paginate(20);
      return view('bans.bans', compact('bans'));
    }
}
