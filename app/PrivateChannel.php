<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivateChannel extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'private_channels';
  protected $primaryKey = 'id'; // unnecessary, just to be sure

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('name', 'type', 'icon', 'icon_url');
  protected $dates = ['created_at', 'deleted'];

  public function recipients()
  {
      return $this->belongsToMany('App\Member', 'private_channels_recipients', 'private_channel_id', 'member_id');
  }

  public function getIcon()
  {
    if ($this->icon_url != "")
      return $this->icon_url;
    return asset('dlb-assets/img/default.jpg');
  }

}
