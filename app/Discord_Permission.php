<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discord_Permission extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'permissions';
  protected $primaryKey = 'value';

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('create_instant_invite', 'kick_members', 'ban_members', 'administrator', 'manage_channels', 'manage_server', 'add_reactions', 'read_messages', 'send_messages', 'send_tts_messages', 'manage_messages', 'embed_links', 'attach_files', 'read_message_history', 'mention_everyone', 'external_emojis', 'connect', 'speak', 'mute_members', 'deafen_members', 'move_members', 'use_voice_activation', 'change_nickname', 'manage_nicknames', 'manage_roles', 'manage_webhooks', 'manage_emojis');

  public function roles() {
    return $this->hasMany('App\Discord_Role', 'permissions', 'value');
  }
}
