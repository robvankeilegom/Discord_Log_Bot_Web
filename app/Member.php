<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'members';
  protected $primaryKey = 'id'; // unnecessary, just to be sure

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('name', 'discriminator', 'avatar', 'bot', 'avatar_url', 'default_avatar', 'default_avatar_url', 'mention', 'display_name');
  protected $dates = ['created_at'];

  public function servers()
  {
      return $this->belongsToMany('App\Server', 'servers_members', 'member_id', 'server_id')->withPivot('joined_at', 'display_name');
  }

  public function roles()
  {
      return $this->belongsToMany('App\Discord_Role', 'member_roles', 'member_id', 'role_id');
  }

  public function getAvatar() {
    if ($this->avatar_url != "")
      return $this->avatar_url;
    return $this->default_avatar_url;
  }
}
