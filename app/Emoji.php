<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emoji extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'emojis';
  protected $primaryKey = 'id'; // unnecessary, just to be sure

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('name', 'require_colons', 'managed', 'url');
  protected $dates = ['created_at'];

  public function server() {
    return $this->hasOne('App\Server', 'id', 'server');
  }
}
