<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
  protected $connection = 'discord_log_db';
  protected $table = 'invites';
  protected $primaryKey = 'id'; // unnecessary, just to be sure

  public $incrementing = false;
  public $timestamps = false;

  protected $fillable = array('name', 'max_age', 'code', 'revoked', 'temporary', 'uses', 'max_uses', 'xkcd', 'url');
  protected $dates = ['created_at'];

  public function inviter() {
    return $this->hasOne('App\Member', 'id', 'inviter');
  }

  public function server() {
    return $this->hasOne('App\Server', 'id', 'server');
  }

  public function channel() {
    return $this->hasOne('App\Channel', 'id', 'channel');
  }
}
