/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  "use strict";

  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  });
  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

  /* Morris.js Charts */
  // Sales chart
  var voice_messages_chart = new Morris.Area({
    element: 'data-chart',
    resize: true,
    xkey: 'x',
    ykeys: ['voiceconnections', 'messages'],
    labels: ['Voice Connections', 'Messages'],
    lineColors: ['#a0d0e0', '#3c8dbc'],
    hideHover: 'auto',
    xLabelFormat: function (d) {
      return d.getDate()+'/'+(d.getMonth()+1);
    },
  });
  voice_messages_chart_addData(voice_messages_chart)


});
