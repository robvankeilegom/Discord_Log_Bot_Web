@extends('la.layouts.app')

@section('htmlheader_title') Roles @endsection
@section('contentheader_title') Role @endsection
@section('contentheader_description') Overview of role: {{ $role->name }} @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <tbody>
            <tr><th>Name</th><td> {{ $role->name }} </td></tr>
            <tr><th>Server</th><td> <a href="{{ route('admin.servers.server', $role->server) }}">{{ App\Server::find($role->server)->name }}</a> </td></tr>
            <tr><th>Color</th><td
              @if ($role->color != 0)
                style="background-color: {{ $role->color() }};"
              @endif
                ></td></tr>
            <tr><th>Hoist</th><td> {{ $role->hoist }} </td></tr>
            <tr><th>Position</th><td> {{ $role->position }} </td></tr>
            <tr><th>Managed</th><td> {{ $role->managed }} </td></tr>
            <tr><th>Mentionable</th><td> {{ $role->mentionable }} </td></tr>
            <tr><th>Created_at</th><td> {{ $role->created_at }} </td></tr>
          </tbody>
        </table>
        <table class="table table-striped">
          <tbody>
            <tr><th colspan=2><h2>Permissions:</h2></th></tr>
            <tr><th>Create Instant Invite</th><td>{{ $permissions->create_instant_invite }}</td>
            <tr><th>Kick Members</th><td>{{ $permissions->kick_members }}</td>
            <tr><th>Ban Members</th><td>{{ $permissions->ban_members }}</td>
            <tr><th>Administrator</th><td>{{ $permissions->administrator }}</td>
            <tr><th>Manage Channels</th><td>{{ $permissions->manage_channels }}</td>
            <tr><th>Manage Server</th><td>{{ $permissions->manage_server }}</td>
            <tr><th>Add Reactions</th><td>{{ $permissions->add_reactions }}</td>
            <tr><th>Read Messages</th><td>{{ $permissions->read_messages }}</td>
            <tr><th>Send Messages</th><td>{{ $permissions->send_messages }}</td>
            <tr><th>Send TTS Messages</th><td>{{ $permissions->send_tts_messages }}</td>
            <tr><th>Manage Messages</th><td>{{ $permissions->manage_messages }}</td>
            <tr><th>Embed Links</th><td>{{ $permissions->embed_links }}</td>
            <tr><th>Attach Files</th><td>{{ $permissions->attach_files }}</td>
            <tr><th>Read Message History</th><td>{{ $permissions->read_message_history }}</td>
            <tr><th>Mention Everyone</th><td>{{ $permissions->mention_everyone }}</td>
            <tr><th>External Emojis</th><td>{{ $permissions->external_emojis }}</td>
            <tr><th>Connect</th><td>{{ $permissions->connect }}</td>
            <tr><th>Speak</th><td>{{ $permissions->speak }}</td>
            <tr><th>Mute Members</th><td>{{ $permissions->mute_members }}</td>
            <tr><th>Deafen Members</th><td>{{ $permissions->deafen_members }}</td>
            <tr><th>Move Members</th><td>{{ $permissions->move_members }}</td>
            <tr><th>Use Voice Activation</th><td>{{ $permissions->use_voice_activation }}</td>
            <tr><th>Change Nickname</th><td>{{ $permissions->change_nickname }}</td>
            <tr><th>Manage Nicknames</th><td>{{ $permissions->manage_nicknames }}</td>
            <tr><th>Manage Roles</th><td>{{ $permissions->manage_roles }}</td>
            <tr><th>Manage Webhooks</th><td>{{ $permissions->manage_webhooks }}</td>
            <tr><th>Manage Emojis</th><td>{{ $permissions->manage_emojis }}</td>
          </tbody>
        </table>
      </div>
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
