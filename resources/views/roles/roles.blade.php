@extends('la.layouts.app')

@section('htmlheader_title') Roles @endsection
@section('contentheader_title') Roles ({{ $roles->total() }}) @endsection
@section('contentheader_description') Overview of roles @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Server</th>
              <th>Color</th>
              <th data-toggle="tooltip" data-placement="bottom" title="Indicates if the role will be displayed separately from other members.">Hoist</th>
              <th>Position</th>
              <th data-toggle="tooltip" data-placement="bottom" title="Indicates if the role is managed by the server through some form of integrations such as Twitch.">Managed</th>
              <th>Mentionable</th>
              <th>Created At</th>
              <th>Deleted At</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($roles as $role)
              <tr>
                <td> <a href="{{ route('admin.roles.role', $role->id) }}">{{ $role->name }}</a> </td>
                <td> <a href="{{ route('admin.servers.server', $role->server) }}">{{ App\Server::find($role->server)->name }}</a> </td>
                <td
                  @if ($role->color != 0)
                    style="background-color: {{ $role->color() }};"
                  @endif
                    ></td>
                <td>
                  @if ($role->hoist == 0)
                    no
                  @else
                    yes
                  @endif
                </td>
                <td> {{ $role->position }} </td>
                <td>
                  @if ($role->managed == 0)
                    no
                  @else
                    yes
                  @endif
                </td>
                <td>
                  @if ($role->mentionable == 0)
                    no
                  @else
                    yes
                  @endif
                </td>
                <td> {{ $role->created_at }} </td>
                <td> {{ $role->deleted }} </td>
                <td>@include('actions.roles')</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{ $roles->links() }}
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
</script>
@endpush
