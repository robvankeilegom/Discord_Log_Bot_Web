<div class="dropdown">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
  <span class="caret"></span></button>
  <ul class="dropdown-menu dropdown-menu-right">
    <li><a href="{{ route('admin.members.server', $server->id) }}"><i class='fa fa-user'></i>  Members</a></li>
    <li><a href="{{ route('admin.channels.server', $server->id) }}"><i class='fa fa-list'></i>  Channels</a></li>
    <li><a href="{{ route('admin.roles.server', $server->id) }}"><i class='fa fa-users'></i>  Roles</a></li>
  </ul>
</div>
