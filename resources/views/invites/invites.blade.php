@extends('la.layouts.app')

@section('htmlheader_title') Invites @endsection
@section('contentheader_title') Invites ({{ $invites->total() }}) @endsection
@section('contentheader_description') Overview of invites @endsection

@section('main-content')
<section class="content">
  <div class="row">
    <section class="col-lg-12">
      <div class="nav-tabs-custom">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Code</th>
              <th>Inviter</th>
              <th>Server</th>
              <th>Channel</th>
              <th>Max Age</th>
              <!-- TODO
              <th>Revoked</th>
              -->
              <th>Temporary</th>
              <th>Uses</th>
              <th>Max Uses</th>
              <th>XKCD</th>
              <th>Url</th>
              <th>Created At</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($invites as $invite)
              <tr>
                <td> {{ $invite->code }} </td>
                <td> <a href="{{ route('admin.members.member', $invite->inviter) }}">{{ App\Member::find($invite->inviter)->name  }}</a> </td>
                <td> <a href="{{ route('admin.servers.server', $invite->server) }}">{{ App\Server::find($invite->server)->name }}</a> </td>
                <td> <a href="{{ route('admin.channels.channel', $invite->channel) }}">{{ App\Channel::find($invite->channel)->name}}</a> </td>
                <td> {{ $invite->max_age/60 }}m </td>
                <!--
                <td> {{ $invite->revoked }} </td>
              -->
                <td>
                  @if ($invite->temporary == 1)
                    yes
                  @else
                    no
                  @endif
                </td>
                <td> {{ $invite->uses }} </td>
                <td> {{ $invite->max_uses }} </td>
                <td> {{ $invite->xkcd }} </td>
                <td> <a target="_blank" href="{{ $invite->url }}">{{ $invite->url }}</a> </td>
                <td> {{ $invite->created_at }} </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{ $invites->links() }}
    </section>
  </div>
</section>
@endsection

@push('styles')
  <link rel="stylesheet" href="{{ asset('dlb-assets/css/style.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

@endpush

@push('scripts')
<script>
</script>
@endpush
